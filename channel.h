#ifndef _CHANNEL_
#define _CHANNEL_

#include "systemc.h"
#include "channel_interface.h"

class channel : public sc_module, public channel_interface {
	private:
		sc_fifo<int> programmer_buffer;
		int top = -1;

	public:
		bool error = false;
		
		channel(sc_module_name name) : 
			sc_module(name),
			programmer_buffer(1)
			{}

		void writeSwitchState(unsigned program) {
			bool writeStatus = programmer_buffer.nb_write(program);
			// cout << "status: " << writeStatus << endl; 
			top = program;
		}
		
		int readSwitchState() {
			// cout << "free: " << isWriteMode() <<endl;
			int v = programmer_buffer.read();
			top = -1;
			// cout << "free (after read): "<< isWriteMode() <<endl;
			return v;
		}

		bool isWriteMode(){
			// cout << "is free? " << programmer_buffer.num_free() <<endl;
			programmer_buffer.num_free() != 0;
		}

		int _top(){
			return top;
		}
};

#endif

