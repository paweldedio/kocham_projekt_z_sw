#include "systemc.h"
#include "proc1.h"
#include "proc2.h"
#include "channel.h"

#if defined(__cplusplus) && (__cplusplus < 201103L)
using std::gets;
#endif

int sc_main(int argc, char* argv[]) {
	sc_clock clock("clk", 5, SC_NS, 0.5, 1, SC_NS);

	channel channel("transmission_channel");

	proc1 proc1("proc1");
	proc1.clockInputSignal(clock);
	proc1.dataPort(channel);

	proc2 proc2("proc2");
	proc2.clockInputSignal(clock);
	proc2.dataPort(channel);

	sc_start();
	return(0);
}