#ifndef _CHANNEL_IF_
#define _CHANNEL_IF_

#include "systemc.h"

class channel_interface : virtual public sc_interface {
	public:
		virtual void writeSwitchState(unsigned program) = 0;
		virtual int readSwitchState() = 0;
		virtual bool isWriteMode() = 0;
		virtual int _top() = 0;
		bool error;
};

#endif

