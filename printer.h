#ifndef _PRINTER_
#define _PRINTER_

#include "systemc.h"

    void printInfoMessage(int program){
        cout << "Cannot start program " << program <<endl;
    }

    void printInfoStartProgram(int value){
        cout << "Turn on (" << value <<")" <<endl;
    }

#endif